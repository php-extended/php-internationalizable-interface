<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-internationalizable-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Internationalizable;

use Stringable;

/**
 * InternationalizableStatusInterface class file.
 * 
 * This class represents internationalizable status of a field. Such field
 * can be allowed to be internationalizable, or not, or both.
 * 
 * @author Anastaszor
 */
interface InternationalizableStatusInterface extends Stringable
{
	
	/**
	 * Gets whether this internationalizable status allows the subject to be
	 * non internationalizable state.
	 * 
	 * @return boolean
	 */
	public function isAllowedNonInternationalizable() : bool;
	
	/**
	 * Gets whether this internationalizable status allows the subject to be
	 * in internationalizable state.
	 * 
	 * @return boolean
	 */
	public function isAllowedInternationalizable() : bool;
	
	/**
	 * Gets whether two internationalizable statuses are equals.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
	/**
	 * Gets the internationalizable status that is obtained by merging two 
	 * internationalizable statuses. This represents the effective relation count
	 * between two models without counting the intermediary model relations.
	 * 
	 * @param InternationalizableStatusInterface $other
	 * @return InternationalizableStatusInterface
	 */
	public function mergeWith(InternationalizableStatusInterface $other) : InternationalizableStatusInterface;
	
}
